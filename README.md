# Agrégation

Dans le dossier [developpements](developpements) vous trouverez les
développements d'informatique écrits pour préparer l'agrégation à l'université
de Rennes 1 / l'ÉNS de Rennes.
Leurs auteurs sont (selon les développements) Antoine Dequay, Julien Duron,
Fabrice Étienne et Louis Noizet.

Dans le dossier [cours](cours) vous trouverez quelques cours qui nous ont été
donnés et dont les professeurs ont accepté que nous partageions le contenu.

Dans le dossier [utiles](utiles) se trouvent le rapport du jury de l'année 2019
(les oraux 2020 n'ont pas eu lieu) et les programmes de 2021. Donc selon toute
probabilité, ça ne sera pas utile aux générations futures.

Dans le dossier [lecons](lecons), vous trouverez des plans de leçons, le plus
souvent écrit à la main, et parfois également tapé en TeX.
