\documentclass[12pt,a4paper]{article}

\setlength{\marginparwidth}{2cm}

%\usepackage[left=2cm,right=2cm,top=2cm,bottom=2.5cm,headsep=15pt]{geometry}
\usepackage[utf8]{inputenc}
\usepackage[french]{babel}
\usepackage[T1]{fontenc}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{stmaryrd}
\usepackage{amsthm}
\usepackage{soul}
\everymath{\displaystyle\everymath{}} % Ça c'est pas beau mais j'ai pas le temps de corriger


\newtheorem{thm}{Théorème}
\newtheorem{defi}{Définition}
\newtheorem{prop}[thm]{Proposition}
\newtheorem{coro}[thm]{Corollaire}
\newtheorem{lem}[thm]{Lemme}
\newtheorem{lemin}{Lemme}[thm]

\newtheorem{exm}[thm]{Exemple}
\newtheorem{rmk}[thm]{Remarque}




\usepackage{fancyhdr}
\pagestyle{fancy}
\fancyhead[L]{Prépa Agreg Option D}
\renewcommand{\footrulewidth}{0.5pt}
\fancyfoot[C]{\thepage}
\fancyfoot[R]{Julien \textsc{Duron} \& Louis \textsc{Noizet} }


\author{Julien \textsc{Duron} \and Louis \textsc{Noizet}}
\title{Démonstration ABR optimal}
\date{\today}

\begin{document}

\maketitle

Soit $U$ un ensemble totalement ordonné et $S = \{s_1, \dots, s_n \}$ une partie de $U$ vérifiant\\ $s_1 < s_2 < \dots < s_n$.\\
Soit $w : U \rightarrow [0, 1]$ une fonction de probabilité sur les éléments de $U$.\\
On étend la relation d'ordre aux parties de $S$ par $X \leq Y \Leftrightarrow \max(X) \leq \min(Y)$.\\
On appelle $\mathcal{A}_{U, S}$ l'ensemble des ABR localement complets sur une partition de $U$ tel que pour tout noeud $x \in A$, $x\subset S$ ou $x \subset U \setminus S$.\\
On notera $p_A(x)$ la profondeur du noeud d'étiquette $x$ dans $A$.\\
Étant donné un ABR $A \in \mathcal{A}$, on notera $v(A) = \sum_{x \in A} p_A(x)w(x)$.\\
Le but est de construire un ABR $A \in \mathcal{A}$ tel que $v(A)$ soit minimal et $A$ a un nombre minimal de sommets.
\begin{rmk}
Tout arbre de $\mathcal{A}$ ne contient que des segments.
\end{rmk}
Soit $0 \leq k \leq n$. On note $F_k$ les ensembles d'éléments compris entre $s_k$ et $s_{k+1}$ strictement.
\begin{lem}
Soit $A\in \mathcal{A}$ minimal sur $(U, S)$, soit $0 \leq k \leq n$ tel que $F_k$ non vide, et $x, y \in F_k$.\\
Alors $x$ et $y$ sont dans le même noeud de $A$.
\end{lem}
\begin{proof}
En effet, considérons l'ensemble des noeuds de $A$ plus grands que $x$ et plus petit que $y$.\\
Cet ensemble est un sous arbre de $A$.\\
Ce sous arbre n'est constitué que d'éléments de $F_k$, donc le remplacer par un unique sommet diminue $v(A)$ sans altérer son appartenance à $\mathcal{A}$.\\
Or $A$ est minimal, donc $x$ et $y$ sont déjà dans le même noeud.
\end{proof}
On en déduit que $A$ est constitué des éléments de $S$ et des $F_k$.
\begin{lem}
Si $A$ est minimal, toutes les feuilles de $A$ sont des éléments de $F_k$ et tous autres noeuds sont des parties de $S$.
\end{lem}
\begin{proof}
	Dans \(F\cup S\) on a \(2 n + 1\) éléments.\\
	Par récurrence forte :\\
	On suppose que pour tout \(k < n\), si A est un ABR sur \(F\cup S\) avec
	\(F\cup S\) = k, on a
	la propriété.
	Au rang n, on a forcément un \(k_i\) à la racine, par parité du sous-arbre
	gauche.
	Par hypothèse de récurrence sur les deux sous-arbres de $A$, on obtient qu'ils contiennent l'ensemble des $F_k$.
	La racine de $A$ est donc bien un élément de $S$.
\end{proof}
\begin{prop}
Soit $\{A_i \ | \ 1 \leq i \leq n \}$ des ABR minimaux pour les ensembles $U < s_i, S < s_i$, restrictions de $U$ et $S$ éléments strictement plus petits que $s_i$.\\ 
Soit $\{B_i \ | \ 1 \leq i \leq n $ les ABR minimaux pour les ensembles $U > s_i, S_i > s_i$.\\
Alors si $i$ vérifie $v(A_i) + v(B_i)$ est minimale, $(A_i, s_i, B_i)$ est minimal.
\end{prop}
\begin{proof}
En effet, on remarque d'une part que tous les arbres de la forme $(A_i, s_i, B_i)$ sont bien dans $\mathcal{A}$.\\
De plus, si $(x, D, G) \in \mathcal{A}$, alors \\
$v((x, D, G)) = v(D) + v(G) + w(x) + \sum_{x \in D \cup G} w(x) = v(D) + v(G) + \sum_{x \in U} w(x) = V(D) + V (G) + 1$.
Donc, si $A = (s_{i_0}, G, D)$ on a nécessairement $v(G) = v(A_{i_0})$ et $V(D) = V(B_{i_0})$, d'où le résultat.
\end{proof}
On en déduit un algorithme dynamique pour calculer $A$.
\begin{verbatim}
On initialise T un tableau de taille (n+1) * (n+1) à l'infini
# T[j][i] représente v(A) ou A est l'arbre minimal pour l'ensemble des
# x de U > s_i et < s_(i + j + 1) avec s_0 := - infini et s_(n+1) := + infini
Pour i allant de 0 à n :
    T[0][i] = w(F_k)
    W[0][i] = w(F_k)
pour j allant de 1 à n :
    pour i allant de 1 à n-j-1 :
        W[j][i] = T[0][i] + T[j-1][i+1] + w(s_(i+1))
        pour k allant de 0 à j :
            T[j][i] = min (T[i][j],  T[k][i] + T[j-1-k][i+k+1])
        T[j][i] += W[j][i]
renvoyer T
\end{verbatim}
D'où une complexité cubique.
\end{document}
