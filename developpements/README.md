# Liste des développements

Chacun des sous-dossiers contient un développement
- [ABROptimal](ABROptimal) : Algorithme pour le problème de l'arbre optimal
- [AhoCorasick](AhoCorasick) : Algorithme d'Aho-Corasick
- [CircuitValue](CircuitValue) : Le problème Circuit-Value est P-complet
- [DeductionNaturelle](DeductionNaturelle) : Exemples de preuves en déduction
	naturelle
- [FFT](FFT) : Transformée de Fourier pour la multiplication de polynômes,
	correction et complexité
- [Hoare](Hoare) : Preuve de correction de la factorielle en logique de Hoare
- [Kruskal](Kruskal) : Algorithme de Kruskal, correction
- [Optimalite](Optimalite) : Optimalité de Ω(n log n) pour un tri par
	comparaison
- [MasterTheorem](MasterTheorem) : Preuve du master theorem pour les algos "diviser pour régner"
- [TriRapide](TriRapide) : Tri rapide
- [Completude](Completude) : Preuve du théorème de complétude pour la logique du
	1er ordre
- [DLO](DLO) : Preuve de l'élimination des quantificateurs pour les ordres
	denses sans limites
- [PremierOrdre](PremierOrdre) : Preuve de l'indécidabilité de la logique du
	1er ordre
- [Presburger](Presburger) : Preuve de la décidabilité de l'arithmétique de
	Presburger
- [UnionFind](UnionFind) : L'algorithme Union-find avec rang et compression de
	chemin s'effectue en temps amorti m·𝛼(n) pour m opérations.
