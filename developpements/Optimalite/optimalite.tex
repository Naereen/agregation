\documentclass{article}

\usepackage{dsfont} % mathds
\usepackage{amsthm} % thm, defi, prop, …
\usepackage{amsmath}
\usepackage{enumitem} % enumerate
\usepackage{amssymb} % square
\usepackage{mathpartir}
\usepackage{todonotes}
\usepackage{stmaryrd}
\usepackage{booktabs}
\usepackage[french]{babel}

\newtheorem{thm}{Théorème}
\newtheorem{defi}{Définition}
\newtheorem{prop}[thm]{Proposition}
\newtheorem{coro}[thm]{Corollaire}
\newtheorem{lem}[thm]{Lemme}
\newtheorem{lemin}{Lemme}[thm]

\newtheorem{exm}{Exemple}
\newtheorem{rmk}[exm]{Remarque}
\newtheorem{exo}{Exercice}

\title{Optimalité de \(n\log_2 n\)}
\author{Louis Noizet}
\date{}
\begin{document}
\maketitle

\begin{lem}\label{lem}
	Soit \(T\) un arbre binaire à \(n\) feuilles. Alors la hauteur moyenne
	d'une feuille est au moins \(\log_2 n\).
\end{lem}

\begin{proof}
	On montre le résultat par récurrence forte sur \(n\in\mathds{N}\).
	Soit \(T=(L, r, R)\) un arbre binaire. Alors, il existe \(k\) tel que
	\(L\) a \(k\) feuilles, et \(R\) en a \(n-k\).
	On note \(h_T(f)\) la hauteur
	de la feuille \(f\) dans l'arbre \(T\) et \(\overline{h}_T\) la hauteur
	moyenne d'une feuille dans l'arbre \(T\). On a~:
	\begin{align*}
		\overline{h}_T&=
		\frac{1}{n}\sum_{f\in \{\text{feuilles de \(T\)}\}}h_T(f)\\
		&= \frac{1}{n}\left[\sum_{f\in \{\text{feuilles de \(L\)}\}}h_T(f)+
		\sum_{f\in \{\text{feuilles de \(R\)}\}}h_T(f)\right]\\
		&= \frac{1}{n}\left[\sum_{f\in \{\text{feuilles de \(L\)}\}}(h_L(f)+1)+
		\sum_{f\in \{\text{feuilles de \(R\)}\}}(h_R(f)+1)\right]\\
		&= \frac{1}{n}\left[\sum_{f\in \{\text{feuilles de \(L\)}\}}h_L(f)+k+
		\sum_{f\in \{\text{feuilles de \(R\)}\}}h_R(f)+(n-k)\right]\\
		&= \frac{1}{n}\left( k\overline{h}_L
		+ \times (n-k)\overline{h}_R\right) + 1
	\end{align*}
	\(L\) et \(R\) ont au moins une feuilles, donc \(k<n\) et \(n-k<n\). On peut
	donc appliquer l'hypothèse de récurrence à \(L\) et \(R\). On a ainsi~:
	\begin{equation}\label{eqn}
		\overline{h}_T \geq 1+\frac{1}{n}(k\log_2 k + (n-k)\log_2(n-k))
	\end{equation}
	La fonction \(x\mapsto x\log_2 x\) est convexe sur \(\mathds{R}_+^*\)
	(sa dérivée seconde est \(x\mapsto \frac{1}{x}\)). Ainsi, par l'inégalité de
	Jensen, on a~:
		\begin{align*}
			k\log_2 k+(n-k)\log_2 (n-k)
			&\geq 2\times\frac{k+(n-k)}{2}\log_2\frac{k+(n-k)}{2}\\
			&= n\log_2\frac{n}{2}\\
			&= n\log_2 n - n
		\end{align*}
		En injectant dans le résultat~\ref{eqn}, on a~:
		\[\overline{h}_T \geq \log_2 n\]
		On conclut par récurrence forte.
\end{proof}

\begin{thm}
	Sous hypothèse d'équiprobabilité des permutations, un algorithme de tri par
	comparaisons s'effectue en \(\Omega(n\times\log_2 n)\) dans le cas moyen
	sur des listes dont les éléments sont distincts.
\end{thm}

\begin{proof}
	Soit \(n\in\mathds{N}\).

	Étant donnée \((x_1,\dots,x_n)\) une liste de \(n\) éléments,
	un algorithme de tri doit trouver la permutation
	\(\sigma\in\mathfrak{S}_n\) telle que
	\(x_{\sigma(1)}\leq x_{\sigma(2)}\leq\cdots\leq x_{\sigma(n)}\).
	Les éléments étant distincts, cette permutation est unique. Et par hypothèse,
	elle est équirépartie.

	Un algorithme de tri par comparaisons peut se modéliser sous forme
	d'un arbre de décision \(T\) (voir figure~\ref{fig:arbre}). Le déroulement d'un
	algorithme sur une permutation donnée consiste à suivre les branches jusqu'à
	la feuille contenant cette permutation.
	Ainsi, toutes les permutations étant équiprobables, l'algorithme s'effectue
	en moyenne en \(\overline{h}_T\) opérations.

	Or, l'arbre \(T\) a au moins \(n!\) feuilles correspondant aux \(n!\)
	permutations. Donc par le lemme~\ref{lem}, tout algorithme s'effectue en
	moyenne en au moins \(\log_2 (n!)\) opérations. Il reste à montrer que
	\(\log_2(n!)\sim n\log_2 n\).
	Par la formule de Stirling~:
	\begin{align*}
		\log_2 (n!) &\sim \log_2(\sqrt{2\pi n}\times\frac{n^n}{e^n})\\
		&= \log_2(\sqrt{2\pi})+\frac{1}{2}\log_2 n+n\log_2 n-n\log_2e\\
		&\sim n\log_2 n.
	\end{align*}

	Ou plus élégamment~: pour tout \(k\geq 2\),
	\[\int_{k-1}^{k}\log t\:\mathrm{d}t \leq \log k
	\leq \int_{k}^{k+1}\log t\:\mathrm{d}t\]
	On note que~:
	\[\log(n!) = \log{\prod_{k=2}^{n}k} = \sum_{k=2}^{n}\log{k}\]
	Ainsi, en sommant, on obtient~:
	\[\int_{1}^{n}\log t\:\mathrm{d}t \leq \log (n!)
	\leq \int_{2}^{n+1}\log t\:\mathrm{d}t\]
	Ainsi,
	\[n\log n-(n+1)\:\mathrm{d}t \leq (n+1)\log (n+1)+(n+1)-2\log 2+2\]
	Or,
	\[\log(n+1)=\log n+\log(1+1/n)\sim\log n+\frac{1}{n}\sim\log n.\]
	Donc,
	\[\log_2(n!)=\log(n!)\times\log_2(e)\sim n\log(n)\log_2(e)=n\log_2(n).\]
\end{proof}

\begin{figure}
	\includegraphics[page=1]{optimalite_fig}
	\caption{Un arbre de décision pour 3 éléments}\label{fig:arbre}
\end{figure}

\end{document}
