\documentclass{article}

\usepackage{dsfont} % mathds
\usepackage{amsthm} % thm, defi, prop, …
\usepackage{amsmath}
\usepackage{enumitem} % enumerate
\usepackage{amssymb} % square
\usepackage{mathpartir}
\usepackage{todonotes}
\usepackage{stmaryrd}
\usepackage{booktabs}
\usepackage[french]{babel}

\newtheorem{thm}{Théorème}
\newtheorem{defi}[thm]{Définition}
\newtheorem{prop}[thm]{Proposition}
\newtheorem{coro}[thm]{Corollaire}
\newtheorem{lem}[thm]{Lemme}

\newtheorem{exm}[thm]{Exemple}
\newtheorem{rmk}[thm]{Remarque}

\newcommand\union{\textsc{Union}}
\newcommand\find{\textsc{Find}}
\newcommand\link{\textsc{Link}}


\DeclareMathOperator\niv{niv}
\DeclareMathOperator\iter{iter}
\DeclareMathOperator\rg{rg}
\DeclareMathOperator\alphop{\alpha}

\title{Étude du tri rapide}
\author{Fabrice Étienne \and Louis Noizet}
\date{}
\begin{document}
\maketitle

\section*{Remarques préalables}

Ce développement est basé sur la preuve présentée dans \emph{Mansuy, Option
informatique MPSI-MP-MP*}.

Il étudie le cas du tri rapide, et donne notamment sa complexité moyenne.

\section{Contexte}

On définit l'algorithme de tri rapide comme ci-dessous. Étant donné un
tableau \(t\) contenant des valeurs comparables (par exemple des entiers),
l'algorithme est censée renvoyer une permutation de ce tableau tel que pour tous
indices \(i,j\) dans le tableau, \(i\leq j\rightarrow t.(i)\leq t.(j)\)

\begin{verbatim}
let partition t g d =
  let i = ref g in
  for j = g to d - 1 do
    if t.(j) < t.(d) then
      (echange t j !i; incr i)
  done;
  echange t d i;
  !i

let trirapide t g d =
  if g < d then
  begin
    p = partition t g d
    trirapide t g (p - 1)
    trirapide t (p + 1) d
  end
\end{verbatim}

\section{Correction}

\subsection{Terminaison}

La fonction de partition se contente de faire une boucle donc elle termine, et
elle renvoie nécessairement un nombre compris entre \(g\) et \(d\).

Quant à la fonction tri rapide, la quantité \(d-g\) est strictement décroissante
lors d'un appel récursif, et le cas négatif termine le programme, donc on est
assuré que l'algorithme termine toujours.

\subsection{Correction}

On a l'invariant de boucle suivant~: lorsque la boucle est au début du tour
\(j\) ou à la fin du tour \(j-1\), on a~:
\[(I_j):\begin{cases}t.(k)< t.(d)&\text{ si }0\leq k<i\\
t.(k)\geq t.(d)&\text{ si }i\leq k < j\end{cases}\]

En effet, si l'on a \((I_j)\), alors
après le tour de boucle suivant, si on n'est pas \(t.(j) < t.(d)\), on aura
\(i' = i + 1\) et l'invariant \((I_{j+1})\) est vérifié. De même si \(t.(j)\geq
t.(d)\), alors on aura \(i' = i\) et l'invariant sera vérifié.

Enfin, le tri rapide est clairement correct sur des entrées de taille 0 ou 1, et
par récurrence forte, il est correct sur toutes les entrées.

\subsection{Complexité dans le cas moyen}

\begin{lem}[Admis]
	Si on a un tableau dont les éléments sont 2 à 2 distincts, et répartis suivant
	une permutation uniforme, alors l'algorithme de partition renverra un élément
	dont la position est uniforme, et les deux sous-tableau auront eux-mêmes une
	permutation uniforme
\end{lem}

\begin{rmk}
	Cette proposition est parfois passée sous silence, mais c'est elle qui permet
	de faire une récurrence, il ne faut donc pas oublier d'en parler, même si ça
	peut éventuellement être mentionné à l'oral uniquement.
\end{rmk}

\begin{lem}[Admis]
	On se donne un tableau de \(n\) éléments distincts répartis suivant une
	permutation uniforme. Alors le temps moyen du partitionnage est linéaire.
\end{lem}

\begin{thm}
	Sous les mêmes hypothèses, le temps moyen du tri rapide est en \(O(n\log
	n)\).
\end{thm}

\begin{proof}
	Par les lemme précédent, en notant \(u_n\) le temps moyen d'exécution du tri
	rapide sur une entrée à \(n\) éléments, on a~:
	\[u_n = \alpha n + \frac{1}{n}\sum_{k=0}^{n-1}(u_k+u_{n-1-k})
		= \alpha n + \frac{2}{n}\sum_{k=0}^{n-1}u_k.\]

	On a ensuite~:
	\begin{align*}
		nu_n-(n+1)u_{n-1}&=nu_n-(n-1)u_{n-1}-2u_{n-1}\\
					&=\alpha n^2+\sum_{k=0}^{n-1}u_k-\alpha(n-1)^2-
						\sum_{k=0}^{n-2}u_k-2u_{n-1}\\
					&=\alpha(2n-1)
	\end{align*}
	En divisant le résultat par \(n(n+1)\) on a

	\begin{align*}
		\frac{u_n}{n+1}-\frac{u_{n-1}}{n}
					&=\alpha\frac{2n-1}{n(n+1)}\\
					&=\alpha\left(\frac{2}{n+1}-\frac{1}{n(n+1)}\right)
	\end{align*}
	Et en sommant cette somme téléscopique, on a~:
	\begin{align*}
		\frac{u_n}{n+1}&=
			\sum_{k=1}^{n}\left(\frac{u_k}{k+1}-\frac{u_{k-1}}{k}\right) + u_0\\
			&=\alpha\sum_{k=1}^{n}\left(\frac{2}{n+1}-\frac{1}{n(n+1)}\right)+1\\
			&\leq2\alpha H_{n+1}+1\\
			&=O(\log n)
	\end{align*}

	Donc \(u_n=O(n\log n)\).
\end{proof}

\end{document}
