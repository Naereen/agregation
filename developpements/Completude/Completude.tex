\documentclass{article}

\usepackage{dsfont} % mathds
\usepackage{amsthm} % thm, defi, prop, …
\usepackage{amsmath}
\usepackage{enumitem} % enumerate
\usepackage{amssymb} % square
\usepackage{mathpartir}
\usepackage{todonotes}
\usepackage{stmaryrd}
\usepackage{booktabs}
\usepackage[french]{babel}

\newtheorem{thm}{Théorème}
\newtheorem{defi}[thm]{Définition}
\newtheorem{prop}[thm]{Proposition}
\newtheorem{coro}[thm]{Corollaire}
\newtheorem{lem}[thm]{Lemme}

\newtheorem{exm}[thm]{Exemple}
\newtheorem{rmk}[thm]{Remarque}

\title{Théorème de complétude}
\author{Louis Noizet}
\date{}
\begin{document}
\maketitle

\section*{Remarques préalables}


\section{Contexte}

On se place dans le système de déduction naturelle à séquents défini par les
règles suivantes~:

\begin{tabular}{cccc}
	\inferrule
		{~}
		{\Gamma,A\vdash A}&
	(ax)&
	\inferrule
		{\Gamma\vdash A}
		{\Gamma,\Delta\vdash A}&
	(w)\\

\inferrule
	{\Gamma,A\vdash B}
	{\Gamma\vdash A\rightarrow B}&
\((\rightarrow i)\)&
\inferrule
	{\Gamma\vdash A\rightarrow B \\ \Gamma\vdash A}
	{\Gamma\vdash B}&
\((\rightarrow e)\)\\

\inferrule
	{\Gamma\vdash A \\ \Gamma\vdash B}
	{\Gamma\vdash A\wedge B}&
\((\wedge i)\)&
\inferrule
	{\Gamma\vdash A_1\wedge A_2}
	{\Gamma\vdash A_j}&
\((\wedge e_j)\)\\

\inferrule
	{\Gamma\vdash A_j}
	{\Gamma\vdash A_1\vee A_2}&
\((\vee i_j)\)&
\inferrule
	{\Gamma\vdash A\vee B \\ \Gamma, A\vdash C \\ \Gamma, B\vdash C}
	{\Gamma\vdash C}&
\((\vee e)\)\\

\inferrule
	{\Gamma,A\vdash\bot}
	{\Gamma\vdash \neg A}&
\((\neg i)\)&
\inferrule
	{\Gamma\vdash A \\ \Gamma\vdash\neg A}
	{\Gamma\vdash\bot}&
\((\neg e)\)\\

\inferrule
	{\Gamma,\neg A\vdash\bot}
	{\Gamma\vdash A}&
\((r.a.a)\)& &\\

\inferrule
	{\Gamma\vdash A[x:=t]}
	{\Gamma\vdash \exists x\cdot A}&
\((\exists i)\)&
\inferrule
	{\Gamma\vdash \exists x\cdot A \\ \Gamma,A\vdash C \\ x\not\in\Gamma,C}
	{\Gamma\vdash C}&
\((\exists e)\)\\

\inferrule
	{\Gamma\vdash A \\ x\not\in\Gamma}
	{\Gamma\vdash \forall x\cdot A}&
\((\forall i)\)&
\inferrule
	{\Gamma\vdash \forall x\cdot A}
	{\Gamma\vdash A[x:=t]}&
\((\forall e)\)\\
\end{tabular}

On va montrer le théorème de complétude.

\section{Preuve du théorème de complétude}

\begin{thm}[Complétude]\label{thm:completude}
	Soit \(\Gamma, F\).
	\[\text{Si }\Gamma\vDash F\text{, alors }\Gamma\vdash F.\]
\end{thm}

\begin{lem}\label{lem:max}
	Si toute théorie maximale cohérente possède un modèle, alors on a la
	complétude.
\end{lem}

\newcommand\model{\ensuremath{\mathfrak{M}}}

\begin{proof}
	Soit \(\Gamma\) une théorie et \(F\) une formule close telles que
	\(\Gamma\vDash F\). Alors, \(\Gamma,\neg F\vDash\bot\). On va montrer que
	\(\Gamma,\neg F\vdash\bot\). Supposons par l'absurde que ce n'est pas le
	cas. Alors \(\Gamma, \neg F\) est cohérente. Par le lemme de Zorn, il
	existe \(\Delta\supseteq\Gamma,\neg F\) telle que \(\Delta\) est maximale
	cohérente. Par hypothèse, \(\Delta\) possède un modèle \model.
	Mais alors \model est aussi un modèle de \(\Gamma,\neg F\), ce qui
	contredit \(\Gamma,\neg F\vDash\bot\). On a donc le résultat attendu.
\end{proof}

\begin{rmk}
	On peut expliciter à l'oral pourquoi le lemme de Zorn s'applique.
\end{rmk}
\begin{proof}
	On se place dans l'ensemble \(\mathcal E\) des théories cohérentes \(T\)
	telles que \(\Gamma,\neg F\subseteq T\). On va montrer que \(\mathcal E\)
	est inductif. Soit \(\mathcal D\) une chaîne de \(\mathcal E\) (i.e un
	sous-ensemble totalement ordonné). Alors \(\Delta = \bigcup \mathcal D\)
	fournit clairement un majorant de \(\mathcal D\). De plus, \(\Delta\) est
	cohérent car toute sous-partie finie de \(\Delta\) est incluse dans un des
	éléments de \(\mathcal D\) et est donc cohérente. Et naturellement,
	\(\Gamma,\neg F\subseteq\Delta\). Donc \(\Delta\in\mathcal E\).
	Donc \(\mathcal E\) est inductif, et donc a un élément maximal.
\end{proof}

\begin{rmk}\label{rmk:eq}
	Soit \(T\) une théorie maximale cohérente. Pour toute formule close \(F\),
	on a les équivalences suivantes~:
	\[T\vdash F\Longleftrightarrow F\in T\]
	\[F\not\in T\Longleftrightarrow \neg F\in T
	\Longleftrightarrow T,F\vdash\bot\]
\end{rmk}

\begin{lem}
	On a la complétude pour le calcul propositionnel
\end{lem}

\newcommand\ol[1]{\overline{#1}}

\begin{proof}
	Soit \(T\) une théorie maximale cohérente. Par le lemme~\ref{lem:max}, il
	suffit de trouver un modèle à \(T\). On propose comme candidat le modèle
	des termes \model, défini de la manière suivante~:
	\[\begin{cases}
		|\model|=\{\overline{t}\mid t \text{ termes clos}\}\\
		f_{\model}^i(\ol{t_1},\dots, \ol{t_n})=\ol{f^i(t_1,\dots,t_n)}\\
		R_{\model}^i(\ol{t_1},\dots, \ol{t_n})
			\Leftrightarrow R^i(t_1,\dots,t_n)\in T
	\end{cases}\]

	On doit montrer que \model est un candidat valide. On va montrer
	par récurrence sur \(F\) que \(\model\vDash F\Leftrightarrow F\in T\).
	\begin{itemize}
		\item Si \(F\) est atomique, \(F=R(t_1,\dots,t_n)\).
			Alors
			\[\model\vDash F\Leftrightarrow
			R_\model(\ol{t_1},\dots\ol{t_n}) \Leftrightarrow
			R(t_1,\dots,t_n)\in T\Leftrightarrow F\in T\]
		\item Si \(F = \neg G\), alors~:
			\[\model\vDash F\Longleftrightarrow
			\model\vDash \neg G\overset{\text{def}}{\Longleftrightarrow}
			\model\not\vDash G\overset{\text{HI}}{\Longleftrightarrow}
			G\not\in T\overset{\text{rmk~\ref{rmk:eq}}}{\Longleftrightarrow}
			\neg G\in T
			\]
		\item Si \(F = \phi \wedge \psi\), alors~:
			\[\model\vDash \phi\wedge\psi\Longleftrightarrow
			\model\vDash \phi\text{ et }\model\vDash\psi\Longleftrightarrow
			\phi\in T\text{ et }\psi\in T\Longleftrightarrow
			\phi\wedge\psi\in T\]
		\item Si \(F = \phi \vee \psi\), alors~:
			\[\model\vDash \phi\vee\psi\Longleftrightarrow
			\model\vDash \phi\text{ ou }\model\vDash\psi\Longleftrightarrow
			\phi\in T\text{ ou }\psi\in T\Longleftrightarrow
			\phi\vee\psi\in T\]
		\item etc
	\end{itemize}
	Il faut justifier les dernières équivalences. En se souvenant que pour
	toute formule on a \(T\vdash F\Longleftrightarrow F\in T\), on vérifie que
	les sens directs ne correspondent qu'à l'application d'une des règles
	d'introduction de la déduction naturelle (\((\wedge i)\) pour le \(\wedge\)
	par exemple). Les sens réciproques en revanche demandent plus de travail.
	En contraposant et en utilisant la remarque~\ref{rmk:eq}, on arrive
	cependant à vérifier qu'elles sont des conséquences des règles
	d'élimination.  Par exemple pour le \(\vee\), la contraposée
	donne~:
	\[\phi\not\in T\text{ et }\psi\not\in T\Rightarrow\phi\vee\psi\not\in T,\]
	c'est-à-dire
	\[T,\phi\vdash\bot\text{ et }T,\psi\vdash\bot\Rightarrow T,\phi \vee
	\psi\vdash\bot,\]
	qui
	est une conséquence de la règle \((\vee e)\)
\end{proof}

\begin{rmk}
	Il ne manque presque rien pour la complétude du premier ordre. Il faudrait
	juste les deux chaîne d'équivalences suivantes
			\[\model\vDash \exists x\phi
			\overset{\text{def}}{\Leftrightarrow}
			\text{\(\exists t\) clos tq }\model\vDash\phi[x:=t]
			\overset{\text{HI}}{\Leftrightarrow}
			\text{\(\exists t\) clos tq }\phi[x:=t]\in T
			\overset{\text{1}}{\Leftrightarrow}
			\exists x \phi\in T\]
			\[\model\vDash \forall x\phi
			\overset{\text{def}}{\Leftrightarrow}
			\text{\(\forall t\) clos tq }\model\vDash\phi[x:=t]
			\overset{\text{HI}}{\Leftrightarrow}
			\text{\(\forall t\) clos tq }\phi[x:=t]\in T
			\overset{\text{2}}{\Leftrightarrow}
			\forall x \phi\in T\]
		Les deux flèches intitulées "\(HI\)" sont des conséquences de l'hypothèse
		d'induction, à condition de faire une induction sur la hauteur de la
		formule plutôt que sur la formule elle-même.

		Les deux flèches intitulées "def" sont vraies par définition.

		Le sens direct de 1 et le sens réciproque de 2 sont immédiats.
		respectivement par \((\exists i)\) et \((\forall e)\).

		En revanche, les deux dernières flèches ne sont pas vraies dans le cas
		général. En effet, affirmer \(\exists x\phi\) ne permet pas dans le cas
		général de dire qu'il existe un terme clos qui en témoigne. On va donc
		s'intéresser au cas où cette propriété est vérifiée.
\end{rmk}

\begin{defi}
	On dit que \(T\) a des témoins de Henkin dans \(T'\), si pour toute formule
	\(\exists x\cdot F\in T\), il existe un terme clos \(t\) tel que
	\(F[x:=t]\in T'\).
\end{defi}

\begin{rmk}
	Les deux flèches restantes sont immédiatement vraies si \(T\) a des témoins
	de Henkin (dans lui-même).
\end{rmk}

\begin{lem}[Lemme de Henkin]
	Tout théorie cohérente \(T_0\) dans un langage \(L_0\) s'étend en une
	théorie cohérente maximale \(T\) ayant des témoins de Henkin
	dans un langage \(L\) où \(L\) est simplement \(L_0\) auquel on a ajouté
	des symboles de constantes.
\end{lem}

\begin{proof}
	Par Zorn, on étend \(T_0\) et \(T_0^m\) maximale cohérente.
	On enrichit le langage \(L_0\) en un langage \(L_1\) en ajoutant un symbole
	de constante \(c_{\exists x\cdot F}\) pour chaque formule
	\(\exists x\cdot F\) dans \(T_0^m\).
	On pose \(T_1\) la théorie de \(L_1\) définie par
	\(T_1=T_0^m\cup\{F[x:=c_{\exists x\cdot F}]\}\).
	\(T_1\) reste cohérente. On étend \(T_1\) en \(T_1^m\) maximale cohérente,
	et on reproduit le même procédé.

	On pose \(T=\bigcup_{i\in\mathds{N}}T_i\), et on peut vérifier qu'elle
	vérifie la conclusion du lemme.
	\begin{itemize}
		\item \(T\) est cohérente sinon on aurait un \(k\) tel que \(T_k\)
			non-cohérente
		\item \(T\) est maximale sinon, on aurait un i tel que \(T_i^m\) non
			maximale.
		\item \(T\) a des témoins de Henkin puisque si \(\exists x\cdot F\in T\),
			alors on a un \(k\) tel que \(\exists x\cdot F\in T_k\) et donc
			\(F[x:=c_{\exists x F}]\in T_{k+1}\)
	\end{itemize}
\end{proof}

\begin{proof}[Démonstration du théorème~\ref{thm:completude}]
	On se donne \(T_0\) maximale cohérente. On l'étend en une théorie \(T\)
	maximale cohérente avec témoins de Henkin. On applique le schéma de preuve
	décrit ci-dessus. On a alors \(\model\) un modèle de \(T\). Mais alors
	\(\model\) est \emph{a fortiori} un modèle de \(T_0\).
\end{proof}

\end{document}
