\documentclass{article}

\usepackage{dsfont} % mathds
\usepackage{amsthm} % thm, defi, prop, …
\usepackage{amsmath}
\usepackage{enumitem} % enumerate
\usepackage{amssymb} % square
\usepackage{mathpartir}
\usepackage{todonotes}
\usepackage{stmaryrd}
\usepackage{booktabs}
\usepackage[french]{babel}

\newtheorem{thm}{Théorème}
\newtheorem{defi}{Définition}
\newtheorem{prop}[thm]{Proposition}
\newtheorem{coro}[thm]{Corollaire}
\newtheorem{lem}[thm]{Lemme}
\newtheorem{lemin}{Lemme}[thm]

\newtheorem{exm}{Exemple}
\newtheorem{rmk}[exm]{Remarque}
\newtheorem{exo}{Exercice}

\newcommand\union{\textsc{Union}}
\newcommand\find{\textsc{Find}}
\newcommand\link{\textsc{Link}}


\DeclareMathOperator\niv{niv}
\DeclareMathOperator\iter{iter}
\DeclareMathOperator\rg{rg}
\DeclareMathOperator\alphop{\alpha}

\title{Complexité amortie pour \textsc{Union-Find}}
\author{Louis Noizet}
\date{}
\begin{document}
\maketitle

\section*{Remarques préalables}

Ce développement est basé sur la preuve présentée dans le \emph{Cormen}.

Il est très long, il faut donc faire des choix si on le présente. Le lemme
\ref{lem:decr_phi} peut par exemple être admis. En revanche, on se gardera
évidemment d'admettre le lemme \ref{lem:find} qui contient l'idée principale de
la démonstration.

\section{Contexte}

\subsection{\textsc{Union-Find}}
\textsc{Union-Find} est une structure abstraite qui sert à décrire des
partitions d'un ensemble d'éléments. Elle a deux opérations de base, l'opération
\textsc{Union} qui réunit deux éléments de la partition, et l'opération
\textsc{Find} qui renvoie un représentant de la classe d'un élément.

Certaines descriptions ajoutent une opération \textsc{Make-Set} qui ajoute un
élément à l'ensemble considéré. Quitte à mettre tous les \textsc{Make-Set} au
début, on pourra ne pas considérer cette opération.

On choisira le cas d'un union-find sur l'ensemble \([0,n-1]\)

L'implémentation étudiée ici est l'implémentation avec rang et compression de
chemin. On donne les algorithmes dans la figure~\ref{fig:unionfind}.

\begin{figure}
\begin{verbatim}
type unionfind =
  { parent: int array,
    rank: int array }

let empty n =
  let parent = Array.make n 0 in
  for i = 0 to n - 1 do
    parent.(i) <- i
  done;
  let rank = Array.make n 0 in
  { parent ; rank }

let find uf u =
  let x = uf.parent.(u) in
  if x = u then x else
  uf.parent.(u) <- find x ; x

let link uf u v =
  if uf.rank.(u) <= uf.rank.(v)
  then uf.parent.(u) <- v
  else uf.parent.(v) <- y ;
  if uf.rank.(u) = uf.rank.(v)
  then uf.rank.(u) = uf.rank.(u) + 1

let union uf u v =
  link uf (uf.find u) (uf.find v)

\end{verbatim}
	\caption{Une implémentation concrète de \textsc{Union-Find}}\label{fig:unionfind}
\end{figure}

\subsection{Ackermann}

On appelle fonction d'Ackermann la fonction \(A\) à deux arguments (on notera
indifféremment \(A(k,n)\) et \(A_k(n)\)) définie de la manière suivante~:
\[A_k(n)=\begin{cases}n+1&,k=0\\A_{k-1}^{n+1}(n)&,k\neq 0\end{cases}\]

On notera de plus \(\alphop\) la fonction appelée fonction inverse d'Ackermann et
définie par~:
\[\alphop(n)=\min\{k\in\mathds{N}\mid A_k(1)\geq n\}.\]
En particulier, on peut noter que \(\alphop(10^{80})=4\), \(10^{80}\) étant le
nombre estimé d'atomes dans l'univers observable. En particulier, on peut
considérer dans un cadre informatique que \(\alphop(n)\) sera toujours plus petit
que 4.

\section{Développement}

\begin{thm}
	Une suite de \(m\) opérations \union{} et \find{} sur un ensemble à \(n\) éléments
	s'effectue dans le pire cas en \(O(m\alphop(n))\)
\end{thm}

On va montrer ce résultat par une méthode à base de potentiel.

Déjà, on peut remplacer le problème de \(m\) opérations \union{} et \find{} par
\(m'\) opérations \link{} et \find. On a alors \(m\leq m'\leq 3m\). On se
placera donc dans ce contexte pour la suite.
On notera \(p\) la fonction parent et \(\rg\) la fonction de rang.

Pour tout \(x\) qui n'est pas une racine et de rang non nul, on définit la
fonction \(\niv\) de la manière suivante~:
\[\niv(x)=\max\{k\mid A_k(\rg(x)) \leq \rg(p(x))\}.\]

En particulier, le rang d'un parent est par construction plus grand que le rang
d'un nœud, donc \(\niv(x) \geq 0\). De plus, on a \(A_{\alphop(n)}(\rg(x))\geq
n\). Or, le rang maximal obtenu est \(n-1\), car le rang d'un nœud est inférieur
par construction à la hauteur de son sous-arbre. Ainsi,
\(0\leq\niv(x)<\alphop(n)\).

Pour tout \(x\) qui n'est pas une racine, on définit ensuite la fonction \(\iter\)
de la manière suivante~:
\[\iter(x)=\max\{i\mid A_{\niv(x)}^{i}(\rg(x)) \leq \rg(p(x))\}.\]

Par définition de \(\niv(x)\), on a \(\iter (x)\geq 1\). De plus, on a~:
\[A_{\niv(x)}^{\rg(x)+1}(\rg(x))=A_{\niv(x)+1}(\rg(x))>\rg(p(x)).\]
Donc on a \(1\leq\iter(x)\leq \rg(x)\).

On définit ensuite le potentiel d'un nœud \(x\) de la manière suivante~:
\[\varphi(x)=
\begin{cases}\alphop(n)\rg(x)&,\text{ si \(x\) une racine ou} \rg(x)=0\\
	(\alphop(n)-\niv(x))\rg(x)-\iter(x)&,\text{ sinon}
\end{cases}\]

On définit ensuite le potentiel total \(\Phi=\sum_x\varphi\)

On vérifie que le potentiel initial est nul, et que \(\varphi\) est toujours
positif.

\begin{lem}\label{lem:decr_phi}
	Lors d'une opération \link{} ou \find{}, pour tout nœud x qui n'est pas un des
	nœuds en arguments du \link{}, \(\varphi(x)\) diminue lors de l'appel. De
	plus, \(si\) x n'est pas une racine, si \(\rg(x)\geq1\) et si \(\niv(x)\) ou
	\(\iter(x)\) change, alors la diminution est stricte.
\end{lem}

\begin{proof}
	On notera avec des \('\) les valeurs après l'opération \link{} ou \find{}.

	Déjà, il convient de remarquer que le rang de \(x\) est inchangé. C'est-à-dire
	qu'on a \(\rg'(x)=\rg(x)\). En effet, seule l'opération \link{} peut changer
	le rang d'un de ses deux arguments.

	Maintenant, si \(x\) est une racine ou si \(\rg(x)=0\), alors
	\[\varphi'(x)=\varphi(x)=\alphop(n)\rg(x).\]

	Sinon, pour changer la valeur de \(\varphi(x)\), il faut changer \(\niv\) ou
	\(\iter\). \(\niv\) ne peut qu'augmenter, suite à une augmentation du rang
	de son parent. Si \(\niv\) augmente, alors
	\begin{align*}
		\varphi'(x)&=(\alphop(n)-\niv'(x))\rg(x)-\iter'(x)\\
						&\leq(\alphop(n)-\niv'(x))\rg(x)-1\\
						&=(\alphop(n)-\niv(x))\rg(x)-\rg(x)-1\\
						&\leq(\alphop(n)-\niv(x))\rg(x)-\iter(x)-1\\
						&=\varphi(x)-1
	\end{align*}
	Puis, si \(\niv\) reste constant, \(\iter\) ne peut qu'augmenter pour les
	mêmes raisons et \emph{mutatis mutandis}, on obtient le résultat attendu.
\end{proof}

\begin{lem}\label{lem:link}
	Le coût amorti d'une opération \link{} est \(O(\alphop)\) dans le pire des
	cas.
\end{lem}

\begin{proof}
	On s'intéresse à une opération \(\link(u,v)\). On supposera sans perte de
	généralité que \(v\) devient parent de \(u\) après l'opération.
	Le coût réel de l'opération est \(O(1)\). Il suffit donc de prouver que
	\(\Phi'-\Phi<\alphop(n)\) dans le pire des cas. Déjà, par le
	lemme~\ref{lem:decr_phi}, on sait que pour tout \(x\) différent de \(u,v\), le
	potentiel diminue. On vérifie trivialement qu'il ne peut également que
	diminuer pour \(u\) puisque \(u\) est une racine avant l'opération.
	Il reste à prouver que \(\varphi'(v)\leq\varphi(v)+\alphop(n)\). Or \(v\) est une
	racine avant et après l'opération, et son rang peut être inchangé ou augmenté
	de 1. Donc
	\[\varphi'(v)-\varphi(v)=\alphop(n)(\rg'(v)-\rg(v))\leq\alphop(n).\]
\end{proof}

\begin{lem}\label{lem:find}
	Le coût amorti d'une opération \find{} est \(O(\alphop)\) dans le pire des
	cas.
\end{lem}

\begin{proof}
	On s'intéresse à une opération \(\find(u)\). On notera \(h\) la longueur du
	chemin de \(u\) à sa racine. La complexité réelle est en \(O(h)\).
	
	Le premier point est que pour tout nœud \(x\), \(\varphi(x)\) diminue. Ce résultat est immédiat par le lemme~\ref{lem:decr_phi}.

	Si \(h\leq\alphop(n)+2\), on a immédiatement le résultat. On va donc supposer
	que \(h>\alphop(n)+2\). On va chercher à construire un ensemble \(X\) de
	cardinal au moins \(h-\alphop(n)-2\) tel que pour tout \(x\in X\),
	\(\phi'(x)<\phi(x)\). On aura alors comme on le désire un coût amorti en
	\(O(h-(h-\alpha(n)-2))=O(\alpha)\).

	Par le lemme~\ref{lem:decr_phi}, il suffit de trouver un ensemble \(X\) de
	cardinal au moins \(h-\alphop(n)-2\) tel que pour tout \(x\in X\), on ait
	\(\rg(x)\neq 0\), \(x\) n'est pas une racine et
	(\(\iter'(x)\neq\iter(x)\) ou \(\niv'(x)\neq\niv(x)\))

	On note \(r\) la racine de l'arbre qui porte \(u\), et \(r'\) son successeur
	dans la branche qui mène vers \(u\).
	On se donne
	\[X=
	\{x|\exists y, x\rightarrow^+y\rightarrow^*r'
	\wedge\niv(y)=\niv(x)
	\wedge\rg(x)\neq 0\}.\]

	Pour qu'un élément de la chaîne \(x\rightarrow^*r\) (chaîne de h + 1 éléments) ne soit pas dans \(X\) il faut qu'il soit \(u\) (car
	on peut avoir \(\rg(u)=0\)) ou \(r\) ou \(r'\) (ce qui empêchera la chaîne
	\(x\rightarrow^+y\rightarrow^*r'\)), ou alors il faut que \(x\) soit le
	dernier nœud de la chaîne \(x\rightarrow^*r'\) à avoir son niveau. Or il y a
	\(\alphop(n)\) niveaux possibles, de \(0\) à \(\alphop(n)-1\).
	Ainsi, \(|X^C|\leq 3+\alpha(n)\). Ainsi, on a \(|X|\geq h-\alpha(n)-2\). De
	plus, prenons \(x\in X\) et \(y\) tel que \(\niv(x)=\niv(y)\) et
	\(x\rightarrow^+y\rightarrow^*r'\). On note \(k=\niv(x)\) et \(i=\iter(x)\).
	On a~:
	\begin{align}
		\rg(p(x))&\geq A_k^i(\rg(x))\\
		\rg(p(y))&\geq A_k(\rg(y))\\
			 \rg(y)&\geq \rg(p(x))
	\end{align}
	(1) et (2) sont vrais par définition, et (3) et vrai car le rang d'un parent
	est toujours strictement plus grand par construction que le rang d'un nœud.
	
	Puis, les rangs sont inchangés, et \(p'(x)=p'(y)\). Donc on a
	\begin{align*}
		\rg'(p'(x)) &=\rg'(p'(y))\\
								&=\rg(p'(y))\\
								&\geq A_k(\rg(y))\\
								&\geq A_k(\rg(p(x)))\\
								&\geq A_k(A_k^i(\rg(x)))\\
								&= A_k^{i+1}(\rg'(x))\\
	\end{align*}
	Donc \(\niv'(x)=\niv(x)=k\Rightarrow\iter'(x)\geq i+1\). Ainsi, \(\niv(x)\) ou
	\(\iter(x)\) 




\end{proof}




\end{document}
